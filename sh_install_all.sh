#!/bin/bash

git clone git@gitlab.com:ursi2021/applications/e-commerce/e-commerce-api.git
git clone git@gitlab.com:ursi2021/applications/decisionnel/decisionnel-api.git
git clone git@gitlab.com:ursi2021/applications/gestion-commerciale/gestion-commerciale-api.git
git clone git@gitlab.com:ursi2021/applications/back-office-magasin/back-office-magasin-api.git
git clone git@gitlab.com:ursi2021/applications/gestion-entrepots/gestion-entrepots-api.git
git clone git@gitlab.com:ursi2021/applications/relation-client/relation-client-api.git
git clone git@gitlab.com:ursi2021/applications/referentiel-produit/referentiel-produit-api.git
git clone git@gitlab.com:ursi2021/applications/monetique-paiement/monetique-paiement-api.git
git clone git@gitlab.com:ursi2021/applications/gestion-promotion/gestion-promotion-api.git
git clone git@gitlab.com:ursi2021/applications/caisse/caisse-api.git
git clone git@gitlab.com:ursi2021/applications/e-commerce/e-commerce-front.git
git clone git@gitlab.com:ursi2021/applications/simulateur.git
git clone git@gitlab.com:ursi2021/applications/dev-db.git
