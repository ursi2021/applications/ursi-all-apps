#!/bin/bash

array=(back-office-magasin-api caisse-api decisionnel-api e-commerce-api e-commerce-front gestion-commerciale-api gestion-entrepots-api gestion-promotion-api monetique-paiement-api referentiel-produit-api relation-client-api simulateur);

for i in ${array[*]}
do
    cd $i;
    git fetch 1> /dev/null;
    git checkout $1 1> /dev/null && git pull 1> /dev/null && echo -e "\033[0;32m--> ${i} was set to and pulled ${1}\033[0m";
    cd ..;
done
