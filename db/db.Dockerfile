FROM mariadb:10.4

ADD dbSchema.sql /docker-entrypoint-initdb.d

ENV MYSQL_USER admin
ENV MYSQL_PASSWORD azerty

ENV MYSQL_ROOT_PASSWORD root
ENV MYSQL_INITDB_SKIP_TZINFO true

EXPOSE 3306