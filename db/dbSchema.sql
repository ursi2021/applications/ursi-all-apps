-- create databases
CREATE DATABASE IF NOT EXISTS `backOfficeMagasin`;
CREATE DATABASE IF NOT EXISTS `Caisse`;
CREATE DATABASE IF NOT EXISTS `decisionnel`;
CREATE DATABASE IF NOT EXISTS `eCommerce`;
CREATE DATABASE IF NOT EXISTS `gestionCommerciale`;
CREATE DATABASE IF NOT EXISTS `gestionEntrepots`;
CREATE DATABASE IF NOT EXISTS `gestionPromotion`;
CREATE DATABASE IF NOT EXISTS `monetique-paiement`;
CREATE DATABASE IF NOT EXISTS `relationClient`;
CREATE DATABASE IF NOT EXISTS `referentielProduit`;

-- create root user and grant rights
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%';