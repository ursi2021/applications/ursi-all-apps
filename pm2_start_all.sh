#!/bin/bash

array=(e-commerce-api decisionnel-api gestion-commerciale-api back-office-magasin-api gestion-entrepots-api relation-client-api referentiel-produit-api monetique-paiement-api gestion-promotion-api caisse-api simulateur);

for i in ${array[*]}
do
        cd $i;
        git pull;
        npm install;
        pm2 --name $i start npm -- start;
        cd ..;
done
