#!/bin/bash


array=(back-office-magasin-api caisse-api decisionnel-api e-commerce-api e-commerce-front gestion-commerciale-api gestion-entrepots-api gestion-promotion-api monetique-paiement-api referentiel-produit-api relation-client-api simulateur);

while true; do
        read -p "This will delete any uncommited work in all projects; Are you sure ? (yes/no) " yn
        case $yn in
                [Yy]* ) break;;
                [Nn]* ) exit;;
                * ) echo "Please answer yes or no.";;
        esac
done

while true; do
        read -p "Sure sure sure ? (yes/no) " yn
        case $yn in
                [Yy]* ) echo resetting...;
                        for item in ${array[*]}
                        do
                                echo resetting $item
                                git -C $item reset --hard;
                        done
                        break;;
                [Nn]* ) exit;;
                * ) echo "Please answer yes or no.";;
        esac
done
echo done.
