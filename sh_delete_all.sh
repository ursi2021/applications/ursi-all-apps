#!/bin/bash

array=(back-office-magasin-api caisse-api decisionnel-api e-commerce-api e-commerce-front gestion-commerciale-api gestion-entrepots-api gestion-promotion-api monetique-paiement-api referentiel-produit-api relation-client-api simulateur);

for item in ${array[*]}
do
        echo deleting $item
        rm -rf $item
done
