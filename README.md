# ursi-all-apps


*A project with scripts for all applications install, reload, delete, etc.
Permet de facilement installer toutes les applications, les lancer en local sur leur dernière version de develop, les arrêter et les supprimer par la suite.*


Bien lire ce readme !
---

---

## I/ Scripts Bash

* `./sh_install_all.sh` **-** clone **tous les repos** dans le directory actuel. Par défaut les repos seront sur la branche develop suite au clone. Ne pas changer les noms des dossiers, tous les scripts et me composent utilisent la liste exacte.

* `./sh_pull_all.sh develop` - pour tous les directories, va sur la branche en question (ici, develop) puis execute un git pull. Pensé pour retourner sur `develop`, ou `master` avec toutes les apps (attention, y compris celle sur laquelle vous travaillez)

  ---

* `./sh_reset_all.sh` **-** dangereux - remet toutes les applications à l'état du dernier commit via un git reset --hard
* `./sh_delete_all.sh` **-** supprime les dossiers des applications (sans supprimer les scripts)

---

## II/ pm2 Environment - guaranteed on VM only

*Ces scripts utilisent pm2 pour lancer en parralèle les applications. En utilisant seulement les scripts vous n'avez pas besoin d'intéragir avec cet utilitaire, mais vous êtes bien entendu invités à comprendre comment il fonctionne.*

* `sudo npm install pm2 -g` -  installe pm2 en global sur la machine pour pouvoir facilement lancer les apps en parallèle (vous verrez c'est facile) 
* `./pm2_start_all.sh` **-** pour toutes les apps qui ont été clonées, fait un git pull, npm install, et start l'app avec pm2
* `./pm2_restart_all.sh` **-** reload toutes les apps avec pm2, en rechargeant également les variables d'environnement
* `./pm2_stop_all.sh` **-** arrête toutes les apps avec pm2
* À utiliser avec parcimonie.

---

## III/ Docker-Compose Environment

*Le docker-compose permet de lancer l’ensemble des applications sur n’importe quel environnement, grâce à docker, et intègre sa propre base de données. **Attention, il faut toujours une certaine quantité de ressources (stockage, mémoire) pour lancer les 10 applications + la base de données.***

### Quelques commandes utiles (à exécuter dans le directory ursi-all-apps)

Assurez vous d’avoir bien installé docker, docker-compose et l’ensemble des repositories (`./sh_install_all.sh` et `./sh_pull_all.sh develop` - voir section 1) avant l’execution.

#### Basics

 - `docker-compose build`: va build toutes les versions des applications. Si une application a déjà été build et n’a pas changé, le build sera récupéré depuis le cache docker.
 - `docker-compose up`: va assurer que la configuration
    - ajouter `-d` pour le mode détaché
    - ajouter `--build` pour demander un build des applications (voir `docker-compose build`)
 - `docker-compose down`: va éteindre tous les conteneurs en cours.

#### Tools

 - `docker-compose up <yourappname> --rebuild`: va reload l’application en question
    - ne pas ajouter `-d` permet de voir les logs de son application (en s'attachant)
    - ajouter `-d` permet de ne pas s'attacher
 - `docker-compose down <yourappname>`: eteint l’application choisie
 - `docker-compose logs <yourappname>`: permet d'afficher les logs d'execution de votre application, si lancée en mode detached
 - `docker-compose ps`: liste les conteneurs actuellement lancés et leur état

Ne pas hésiter à lire la documentation docker-compose si vous avez besoin de plus de fonctionnalités, et ne pas hésiter à poser des questions sur le channel #demande-aide-devops si besoin