#!/bin/bash

array=(e-commerce-api decisionnel-api gestion-commerciale-api back-office-magasin-api gestion-entrepots-api relation-client-api referentiel-produit-api monetique-paiement-api gestion-promotion-api caisse-api simulateur);

for i in ${array[*]}
do
        echo restart $i
        pm2 restart --update-env $i
done
